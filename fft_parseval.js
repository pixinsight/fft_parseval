console.show();

// THE IDs IDENTIFYING THE SOURCE IMAGE, THE REAL-PART FFT, THE IMAGINARY-PART FFT
let imageId = "I";
let FFT_R_Id = "FFT_R";
let FFT_I_Id = "FFT_I";

// aux: returns a float value for a given keyword
function findKeyValue(keywords, key) {
   for (let i=0; i<keywords.length; i++)
      if (keywords[i].name == key)
         return parseFloat(keywords[i].value);
   return undefined;
}

//

let img = ImageWindow.windowById(imageId);
let FFTR = ImageWindow.windowById(FFT_R_Id);
let FFTI = ImageWindow.windowById(FFT_I_Id);

let mimg = img.mainView.image.toMatrix();
let mr = FFTR.mainView.image.toMatrix();
let mi = FFTI.mainView.image.toMatrix();

let rmin = findKeyValue(FFTR.keywords, "PIDFTMIN");
let rmax = findKeyValue(FFTR.keywords, "PIDFTMAX");

let imin = findKeyValue(FFTI.keywords, "PIDFTMIN");
let imax = findKeyValue(FFTI.keywords, "PIDFTMAX");

// renormalize the scale
let mrn = new Matrix(mr);
mrn.mul(rmax-rmin);
mrn.add(rmin);

let min = new Matrix(mi);
min.mul(imax-imin);
min.add(imin);

let N = mimg.sum();
let CN = N / mrn.at(0,0);

// RESCALED VALUES TO CONFORM TO THE PARSEVAL THEOREM
let mrN = new Matrix(mrn); // the real part FFT rescaled component
let mrN = new Matrix(min); // the imaginary part FFT rescaled component
mrN.mul(CN);
miN.mul(CN);

console.noteln("mr  [0][0] : ", mr.at(0,0));
console.noteln("mrn [0][0] : ", mrn.at(0,0));
console.noteln("mrN[0][0] : ", mrN.at(0,0));

console.noteln("mi  [0][0] : ", mi.at(0,0));
console.noteln("min [0][0] : ", min.at(0,0));
console.noteln("miN[0][0] : ", miN.at(0,0));


